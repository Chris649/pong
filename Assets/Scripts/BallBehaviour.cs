﻿using UnityEngine;
using System.Collections;

public class BallBehaviour : MonoBehaviour {

    public float speed;
    public Rigidbody2D rb2d;

    private Vector2 oldVelocity, newVelocity;

    // Use this for initialization
    void Start () {
        Vector2 angle = (Random.insideUnitCircle);
        angle = angle / angle.magnitude; //Make the angle into a unit vector
        newVelocity = (speed * angle);
        rb2d.velocity = newVelocity;
	}

    void FixedUpdate() {

        //Ray coming from the center of the object
        Ray2D[] rays = new Ray2D[3];
        rays[0] = new Ray2D(rb2d.position + (rb2d.velocity / rb2d.velocity.magnitude)*0.3f, rb2d.velocity / rb2d.velocity.magnitude);
        Vector2 normal = (rb2d.velocity / rb2d.velocity.magnitude);
        float temp = normal.x;
        normal.x = -normal.y;
        normal.y = temp;
        rays[1] = new Ray2D(rb2d.position + normal * 0.3f, rb2d.velocity / rb2d.velocity.magnitude);
        rays[2] = new Ray2D(rb2d.position + normal * -0.3f, rb2d.velocity / rb2d.velocity.magnitude);
        RaycastHit2D[] hits = new RaycastHit2D[3];

        bool collided = false;
        //For recording the velocity
        for (int i = 0; i < hits.Length; i++)
        {
            if (collided)
                break;
            try
            {
                hits[i] = Physics2D.Raycast(rays[i].origin, rays[i].direction, 0.0001f);
                //Debug.Log("tag: " + hit.collider.gameObject.tag);
                if (hits[i].collider.gameObject.tag == "Player")
                {
                    oldVelocity = newVelocity;
                    newVelocity = new Vector2(rb2d.velocity.x, -rb2d.velocity.y);
                    rb2d.velocity = newVelocity;
                    collided = true;
                }
            }
            catch (System.NullReferenceException e) { }
        }
        //If it hits a wall
        if (rb2d.position.x > 2.65 || rb2d.position.x < -2.65)
        {
            rb2d.velocity = new Vector2(-rb2d.velocity.x, rb2d.velocity.y);
        }
        if (rb2d.position.y > 4.85 || rb2d.position.y < -4.85)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, -rb2d.velocity.y);
        }
    }
}