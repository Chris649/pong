﻿using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour {

    public float speed;

    public Rigidbody2D rb2d;

    void Start() {

        //rb2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate() {

        if (Input.touchCount > 0)
        {

            foreach (Touch t in Input.touches)
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(t.position);
                Vector2 position2D;
                position2D.x = position.x;
                position2D.y = position.y;

                if (rb2d.position.y < 0)
                {

                    if (position2D.y < 0)
                    {

                        //if (position2D.x > rb2d.position.x)
                        if (position2D.x > 0)
                        {
                            Vector2 movement = new Vector2(1, 0);
                            rb2d.velocity = (movement * speed);
                            //rb2d.AddForce(movement * speed);
                        }
                        //if (position2D.x < rb2d.position.x)
                        if (position2D.x < 0)
                        {
                            Vector2 movement = new Vector2(-1, 0);
                            rb2d.velocity = (movement * speed);
                            //rb2d.AddForce(movement * speed);
                        }

                    }
                } else if (rb2d.position.y > 0)
                {

                    if (position2D.y > 0)
                    {

                        //if (position2D.x > rb2d.position.x)
                        if (position2D.x > 0)
                        {
                            Vector2 movement = new Vector2(1, 0);
                            rb2d.velocity = (movement * speed);
                            //rb2d.AddForce(movement * speed);
                        }
                        //if (position2D.x < rb2d.position.x)
                        if (position2D.x < 0)
                        {
                            Vector2 movement = new Vector2(-1, 0);
                            rb2d.velocity = (movement * speed);
                            //rb2d.AddForce(movement * speed);
                        }

                    }
                }
            }
        }
        if (rb2d.position.x > 2.05)
        {
            rb2d.velocity = new Vector2(0, 0);
            rb2d.MovePosition(new Vector2((float)2.05, rb2d.position.y));
        }
        if (rb2d.position.x < -2.05)
        {
            rb2d.velocity = new Vector2(0, 0);
            rb2d.MovePosition(new Vector2((float)-2.05, rb2d.position.y));
        }
    }

}